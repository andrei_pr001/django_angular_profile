import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserInfo} from '../../interfaces/userinterface';
import {AuthService} from '../../services/auth.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {delay} from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {

  public createMode: boolean = false;

  // @ts-ignore
  public user: UserInfo;
  public message: string = '';
  public errorMessage: string = '';
  private routeSub: Subscription;
  private userId = 0;
  // @ts-ignore
  form: FormGroup;

  constructor(
    private http: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.routeSub = this.route.params.subscribe( params => {
      // console.log(params)
      // console.log(params['id'])
      this.userId = params['id'];
    })
  }

  ngOnInit(): void {
    if (this.userId != 0) {
      console.log('Get user card')
      this.http.getUser(this.userId).subscribe((user: UserInfo) => {
        this.user = user

        this.form.patchValue({
          userName: this.user.username ? this.user.username : '',
          firstName: this.user.first_name ? this.user.first_name : '',
          lastName: this.user.last_name ? this.user.last_name : '',
          email: this.user.email ? this.user.email : '',
          lastLogin: this.user.last_login ? this.user.last_login : '',
          id: this.user.id ? +this.user.id : 0,
        })
      })
    }
    else {
      console.log('Create mode')
      this.createMode = true
    }

    this.form = new FormGroup({
      lastLogin: new FormControl(''),
      userName: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      id: new FormControl(''),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ])
    })
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  submit() {
    console.log(this.form.value)
    if (this.createMode && this.form.valid){
      // this.user.password = this.form.value.password
      let user: UserInfo = {
        'username': this.form.value.userName,
        'password': this.form.value.password,
        'first_name': this.form.value.firstName,
        'last_name': this.form.value.lastName,
        'email': this.form.value.email,
        'id': this.form.value.id
      }
      this.http.createUser(user).subscribe((user: UserInfo) => {
        console.log('User create: ', user)
        this.message = 'User created successfully';
        setTimeout(() =>
          {
            this.message = '';
            this.router.navigate(['/users']);
          },
          3000);
      }, error => {
        // console.log('Error: ', error)
        this.errorMessage = `User creation error: ${JSON.stringify(error.error)}`
        setTimeout(() =>
          {
            this.errorMessage = '';
            // this.form.reset();
          },
          5000);
      })
    }
    else {
      this.user.id = this.form.value.id
      this.user.username = this.form.value.userName
      this.user.first_name = this.form.value.firstName
      this.user.last_name = this.form.value.lastName
      this.user.email = this.form.value.email
      this.user.last_login = this.form.value.lastLogin ? this.form.value.lastLogin : null
      this.http.updateUser(this.user).subscribe((user: UserInfo) => {
        console.log('User update: ', user);
        this.message = 'User update successfully';
        setTimeout(() =>
          {
            this.message = '';
          },
          3000);
      }, error => {
        this.errorMessage = `User update error: ${JSON.stringify(error.error)}`
        setTimeout(() =>
          {
            this.errorMessage = '';
          },
          5000);
      })
    }
  }
}
