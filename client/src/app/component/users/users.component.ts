import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {UserInfo} from '../../interfaces/userinterface';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  // @ts-ignore
  public users: Array<UserInfo>;

  constructor(
    private http: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.http.getUsers().subscribe((users: Array<UserInfo>) => {
      this.users = users
    })
  }

  addUser() {
    this.router.navigate(['/user', '0'])
  }

  openUserCard(id: number) {
    this.router.navigate(['/user', +id])
  }
}
