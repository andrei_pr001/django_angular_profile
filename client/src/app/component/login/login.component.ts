import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {Subject} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  public errorMessage: string = '';
  public commonErrorMessage: string = 'Something went wrong. Please contact support.';

  // @ts-ignore
  public form: FormGroup;

  constructor(
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.authService.logout();

    this.form = new FormGroup({
      'username': new FormControl(''),
      'password': new FormControl('')
    })
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  onSubmit() {
    this.authService.login(this.form.value.username, this.form.value.password).subscribe(
      response => {
        if (response.token) {
          this.router.navigate(['/']);
        } else {
          this.errorMessage = this.commonErrorMessage;
        }
      },
      // @ts-ignore
      (error: HttpErrorResponse) => {
        if (error.status === 400) {
          return this.errorMessage = 'Incorrect username or password combination';
        }
        this.errorMessage = this.commonErrorMessage;
      }
    );
  }

}
