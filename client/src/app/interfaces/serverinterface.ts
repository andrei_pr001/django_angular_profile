export interface ServerResponseInterface {
  token: string;
  email: string;
}
