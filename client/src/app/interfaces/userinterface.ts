export interface UserInfo {
  id: number;
  last_login?: string;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
  password?: string;
}
