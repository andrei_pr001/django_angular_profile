import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {ServerResponseInterface} from '../interfaces/serverinterface';
import {Observable, throwError} from 'rxjs';
import {UserInfo} from '../interfaces/userinterface';
import {catchError, map} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private jwtHelper: JwtHelperService;
  public isAuthenticated: boolean = false;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.jwtHelper = new JwtHelperService();
    this.isAuthenticated = this.isAuth();
  }

  static getToken() {
    return localStorage.getItem('token');
  }

  public login(username: string, password: string) {
    const body = JSON.stringify({username, password});

    return this.http.post<ServerResponseInterface>(`api/auth/login`, body)
      .pipe(
        map(response => {
        if (response.token) {
          localStorage.setItem('token', response.token);
          localStorage.setItem('user', username);
          this.isAuthenticated = true;
        }
        return response;
      }),
      catchError( err => {
        return throwError(err)
      })
    );
  }

  public logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.isAuthenticated = false
    this.router.navigate(['/login']);
  }

  public isAuth(): boolean {
    // @ts-ignore
    return !this.jwtHelper.isTokenExpired(localStorage.getItem('token'));
  }

  public getUsers(): Observable<UserInfo[]> {
    return this.http.get<UserInfo[]>('api/auth/user')
      .pipe(
        catchError( err => {
          return throwError(err)
        })
      )
  }

  public getUser(id: number): Observable<UserInfo> {
    return this.http.get<UserInfo>(`api/auth/user/${id}`)
      .pipe(
        catchError( err => {
          return throwError(err)
        })
      )
  }

  public updateUser(user: UserInfo): Observable<UserInfo> {
    return this.http.patch<UserInfo>(`api/auth/user/${user.id}/`, JSON.stringify(user))
  }

  public createUser(user: UserInfo): Observable<UserInfo> {
    return this.http.post<UserInfo>('api/auth/user/', JSON.stringify(user))
      .pipe(
        catchError( error => {
          // console.log('Error catch: ', error)
          return throwError(error)
        })
      )
  }

}
