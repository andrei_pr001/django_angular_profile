import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../environments/environment';
import {AuthService} from '../services/auth.service';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    let baseUrl = environment.baseUrl;
    let authorization = `JWT ${AuthService.getToken()}`;
    let requestData: {};
    if (request.body instanceof FormData) {
      requestData = {
        url: `${baseUrl}${request.url}`,
        setHeaders: {
          Authorization: authorization
        }
      };
    } else {
      requestData = {
        url: `${baseUrl}${request.url}`,
        setHeaders: {
          Authorization: authorization,
          'Content-Type': 'application/json'
        }
      };
    }
    request = request.clone(requestData);
    return next.handle(request);
  }
}
