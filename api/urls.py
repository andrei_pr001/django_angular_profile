from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from api.auth.views import UserList, UserViewSet

from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'user', UserViewSet, basename='Users')

urlpatterns = [
    path('login', obtain_jwt_token),
    path('refresh_token', refresh_jwt_token),
    # path('user/list', UserList.as_view())
]

urlpatterns += router.urls
