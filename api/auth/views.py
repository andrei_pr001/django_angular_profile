from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from django.contrib.auth.models import User
from api.auth.serializers import UserSerializer

from django.shortcuts import get_object_or_404

# Create your views here.


class UserList(ListAPIView, APIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    http_method_names = ['get']


class UserViewSet(viewsets.ViewSet):
    serializer_class = UserSerializer

    def list(self, request):
        queryset = User.objects.all()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=kwargs['pk'])
        serializer = UserSerializer(user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
